package week8;

public class Resource
{
	private int numResources;
	private final int MAX = 5;
	public Resource(int startLevel)
	{
		numResources = startLevel;
	}
	public int getLevel()
	{
		return numResources;
	}
	public synchronized addOne()
	{
		try {
			while (numResources >= MAX)  // good thing here, is that 
				wait();  // we will wait the shortest time possible.
			// If we didn't use synchronized, wait() and notify()
			// we would use some kind of loop, asking all the time,
			// whether the resources are filled up to max.
			numResources++;
			//'Wake up' any waiting consumer...
			notifyAll();
		}
		catch (InterruptedException interruptEx) {
			System.out.println(interruptEx);
		}
		return numResources;
	}
	public synchronized int takeOne()
	{
		try {
			while (numResources == 0)
				wait();  // clients who use resources will wait here
			// When a new resource has been produced, notifyAll() is called
			// and the client can proceed here...
			numResources--;
			//'Wake up' waiting producer... if the producer is waiting
			// because the stock is full of resources.
			notify(); 
		}
		catch (InterruptedException interruptEx) {
			System.out.println(interruptEx); 
		}
		return numResources;
	}
}

